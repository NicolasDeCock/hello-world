﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace MultiThreading_Opdracht_1
{
    class Program
    {
        static void Main(string[] args)
        {            
            Thread bThread = new Thread(new ThreadStart(B));
            bThread.Start();


            bool bT = bThread.Join(50);
            if (bT)
            {
                Console.WriteLine("B is binnen de 50 ms!");
            }
            else
            {
                Console.WriteLine("B niet binnen 50 ms");
            }

            Console.Write("\nPress any Key to continue...");
            Console.ReadKey();
        }

        static void A()
        {
            for (int i = 0; i <= 100; i++)
            {
                Console.WriteLine("A" + i);
            }
        }

        static void B()
        {
            Thread aThread = new Thread(new ThreadStart(A));
            aThread.Start();

            bool aT = aThread.Join(5);
            if (aT)
            {
                Console.WriteLine("A is binnen de 50 ms!");
            }
            else
            {
                Console.WriteLine("A niet binnen de 5 ms!");
            }

            for (int i = 0; i <= 100; i++)
            {
                Console.WriteLine("B" + i);
            }
        }
    }
}
